package cl.duoc.soylenhip_prueba1.Entidades;

/**
 * Created by DUOC on 21-04-2017.
 */

public class Usuario {
    private String Usuario;
    private String Password;

    public String getUsuario() {
        return Usuario;
    }

    public void setUsuario(String usuario) {
        Usuario = usuario;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }
}
