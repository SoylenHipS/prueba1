package cl.duoc.soylenhip_prueba1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

import cl.duoc.soylenhip_prueba1.BD.BaseDatos;
import cl.duoc.soylenhip_prueba1.Entidades.Usuario;

public class ListadoUsuarioActivity extends AppCompatActivity {

    private ListView lista;
    private ArrayList<Usuario> dataSource;
    private static ArrayList<Usuario> us = BaseDatos.obtieneListadoUsuarios();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listado_usuario);

        lista=(ListView)findViewById(R.id.listado);

    }
}
