package cl.duoc.soylenhip_prueba1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import cl.duoc.soylenhip_prueba1.BD.BaseDatos;
import cl.duoc.soylenhip_prueba1.Entidades.Usuario;

public class RegistroActivity extends AppCompatActivity {

    private EditText lblPassword, lblUsu, lblRepPass;
    private Button btnEntrar, btnLogin;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        lblUsu=(EditText)findViewById(R.id.lblUser);
        lblPassword=(EditText)findViewById(R.id.lblPasswprd);
        lblRepPass=(EditText)findViewById(R.id.lblRepPass);
        btnEntrar = (Button)findViewById(R.id.btnRegistrar);
        btnLogin = (Button)findViewById(R.id.btnLogin);


        btnEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registrarUsuario();
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent l = new Intent(RegistroActivity.this, LoginActivity.class);
                startActivity(l);
            }
        });

    }

    private void registrarUsuario() {
        String Mensaje ="";
        if (lblUsu.getText().toString().length()>1){
            Mensaje +="Ingres Usuario \n";
        }
        if (lblPassword.getText().toString().length()>1){
            Mensaje +="Ingres Contraseña \n";
        }
        if(!lblRepPass.getText().toString().equals(lblPassword.getText().toString())) {
            Mensaje += "Claves no coinsiden \n";
        }

        if (Mensaje.equals("")){
            Usuario us = new Usuario();
            us.setUsuario(lblUsu.getText().toString());
            us.setPassword(lblPassword.getText().toString());
            BaseDatos.agregarUsuario(us);
            Toast.makeText(RegistroActivity.this, "Registro Correcto", Toast.LENGTH_SHORT).show();
            Intent l = new Intent(RegistroActivity.this, LoginActivity.class);
            startActivity(l);
        }
        }

}
