package cl.duoc.soylenhip_prueba1;

import android.content.Intent;
import android.graphics.ImageFormat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.List;

import cl.duoc.soylenhip_prueba1.BD.BaseDatos;
import cl.duoc.soylenhip_prueba1.Entidades.Usuario;


public class LoginActivity extends AppCompatActivity {

    private EditText lblUsuario, lblPass;
    private Button btnEntrar, btnRegistrarse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        lblUsuario=(EditText) findViewById(R.id.lblUsuario);
        lblPass=(EditText) findViewById(R.id.lblPass);
        btnEntrar=(Button)findViewById(R.id.btnEntrar);
        btnEntrar=(Button)findViewById(R.id.btnRegistrarse);

        btnEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v.getId() == R.id.btnEntrar){
                    if (!lblUsuario.getText().toString().equals("Soylen") && lblPass.getText().toString().equals("SOL.ITA1989")){
                        Toast.makeText(LoginActivity.this, "Bienvenido", Toast.LENGTH_SHORT).show();
                        Intent i = new Intent(LoginActivity.this, ListadoUsuarioActivity.class);
                        startActivity(i);
                    }
                    else
                    {
                        Toast.makeText(LoginActivity.this, "Usuario o Contraseña incorrecto", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

       /* btnRegistrarse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v.getId() == R.id.btnRegistrarse){
                    Intent s = new Intent(LoginActivity.this, RegistroActivity.class);
                    startActivity(s);
                }
            }
        });*/

    }
}
