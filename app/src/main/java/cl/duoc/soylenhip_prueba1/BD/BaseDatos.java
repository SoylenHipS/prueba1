package cl.duoc.soylenhip_prueba1.BD;

import java.util.ArrayList;

import cl.duoc.soylenhip_prueba1.Entidades.Usuario;

/**
 * Created by DUOC on 21-04-2017.
 */

public class BaseDatos {
    private static ArrayList<Usuario> values = new ArrayList<>();
    public static void agregarUsuario(Usuario usuario){
        values.add(usuario);
    }

    public static ArrayList<Usuario> obtieneListadoUsuarios(){
        return values;
    }

}
